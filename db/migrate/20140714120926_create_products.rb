class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name 
      t.string :brand
      t.string :prod_type
      t.string :price
      t.string :discount
      t.string :description
      t.string :ratings
      t.string :emi
      t.string :delivery_type
      t.string :offers
      t.string :cash_on_delivery
      t.string :guarantee
      t.string :specs
      t.string :review
      t.string :instock
      t.string :out_of_stock
      t.string :prod_image1
      t.string :prod_image2
      t.string :prod_image3
      t.string :prod_image4
      t.string :prod_image5
      t.belongs_to :category
      t.belongs_to :sub_category
      t.timestamps
    end
  end
end
