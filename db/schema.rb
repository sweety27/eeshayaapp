# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140716122706) do

  create_table "categories", force: true do |t|
    t.string   "name"
    t.string   "cat_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
  end

  create_table "products", force: true do |t|
    t.string   "name"
    t.string   "brand"
    t.string   "prod_type"
    t.string   "price"
    t.string   "discount"
    t.string   "description"
    t.string   "ratings"
    t.string   "emi"
    t.string   "delivery_type"
    t.string   "offers"
    t.string   "cash_on_delivery"
    t.string   "guarantee"
    t.string   "specs"
    t.string   "review"
    t.string   "instock"
    t.string   "out_of_stock"
    t.string   "prod_image1"
    t.string   "prod_image2"
    t.string   "prod_image3"
    t.string   "prod_image4"
    t.string   "prod_image5"
    t.integer  "category_id"
    t.integer  "sub_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sub_categories", force: true do |t|
    t.string   "name"
    t.string   "sub_cat_type"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
