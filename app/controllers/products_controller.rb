class ProductsController < ApplicationController
  def index
    @products = Product.all
  end

  def new
    @product = Product.new 
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to @product
    else
      render 'new'
    end
  end

  def edit
    @product  = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])

    if @product.update(product_params)
      redirect_to @product
    else
      render 'edit'
    end
  end

  def show
    @product = Product.find(params[:id]) 
  end
  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    redirect_to products_path
  end

  private
  def product_params
    params.require(:product).permit(:name,:brand,:prod_type,:price,:discount,:description,:ratings,:emi,:delivery_type,:offers,:cash_on_delivery,:guarantee,:specs,:review,:instock,:out_of_stock,:category_id,:sub_category_id)
  end
end
