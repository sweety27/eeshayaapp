class SubCategoriesController < ApplicationController
  def index
    @subcategories = SubCategory.all
  end

  def new
    @subcategory = SubCategory.new 
  end

  def create
    @subcategory = SubCategory.new(sub_category_params)
    if @subcategory.save
      redirect_to @subcategory
    else
      render 'new'
    end
  end

  def edit
    @subcategory  = SubCategory.find(params[:id])
  end

  def update
    @subcategory = SubCategory.find(params[:id])

    if @subcategory.update(sub_category_params)
      redirect_to @subcategory
    else
      render 'edit'
    end
  end

  def show
    @subcategory = SubCategory.find(params[:id]) 
  end
  def destroy
    @subcategory = SubCategory.find(params[:id])
    @subcategory.destroy

    redirect_to sub_categories_path
  end

  private
  def sub_category_params
    params.require(:sub_category).permit(:name, :sub_cat_type,:category_id)
  end
end
