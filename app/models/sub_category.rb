class SubCategory < ActiveRecord::Base
  has_many :products, dependent: :destroy
  belongs_to :category
  SUBCATEGORY_TYPES = ["a", "b"]
end
