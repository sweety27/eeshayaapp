class Category < ActiveRecord::Base
  has_many :products, dependent: :destroy
  has_many :sub_categories, dependent: :destroy
  CATEGORY_TYPES = ["a", "b"]
  mount_uploader :image, ImageUploader
end
